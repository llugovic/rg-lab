(in-package #:rg-lab3)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Live

(defun now ()
  (/ (get-internal-real-time) 1000))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Hosts

(defun window-title ()
  (cepl.host:surface-title (cepl:current-surface)))

(defun (setf window-title) (title)
  (cepl.host:set-surface-title (cepl:current-surface) title))

(defun viewport-size ()
  (s~ (cepl:viewport-params-to-vec4 (cepl:current-viewport)) :zw))

(defun (setf viewport-size) (size)
  (destructuring-bind (width height) size
    (cepl:with-cepl-context (ctx)
      (cepl.viewports::%set-current-viewport
       ctx (cepl.viewports:make-viewport (list width height) '(0 0))))))

(defun window-size ()
  (cepl.host:window-size (cepl:current-surface)))

(defun (setf window-size) (size)
  (destructuring-bind (width height) size
    (cepl.host:set-surface-size (cepl:current-surface) width height)
    (setf (viewport-size) size)))

(asdf:defsystem #:rg-lab3
  :description "RG Lab3"
  :author "Lovro Lugović <lovro.lugovic@fer.hr>"
  :license  "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on ("cepl"
               "skitter"
               "cepl.sdl2"
               ;; cepl.skitter.sdl2 *must* to be loaded in order for mouse
               ;; events to work.
               "cepl.skitter.sdl2"
               "rtg-math"
               "rtg-math.vari"
               "dendrite"
               "dirt"
               "livesupport")
  :components ((:file "package")
               (:file "util")
               (:file "cepl-util")
               (:file "camera")
               (:file "main")))

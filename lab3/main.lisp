(in-package #:rg-lab3)

(defvar *running* nil)
(defvar *mouse-ang* (v! 0 0))
(defvar *camera* nil)

(defvar *square-stream* nil)
(defvar *cube-stream* nil)
(defvar *box-stream* nil)

(defvar *texture-1-fbo* nil)
(defvar *texture-2-fbo* nil)
(defvar *texture-3-fbo* nil)
(defvar *cubemap-1* nil)
(defvar *cubemap-2* nil)
(defvar *cubemap* nil)

(defvar *dir* (asdf:system-relative-pathname :rg-lab3 ""))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; NOTE: Demo
;;;
;;; - Lisp
;;; - Macros
;;; - CEPL
;;; - Shader modification
;;; - Sampler modification
;;; - Kernels
;;; - Mandelbrot background
;;; - Cubemap

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Shaders

;;; Full

(defstruct-g pos-norm-tex
  (position :vec3 :accessor pos)
  (normal :vec3 :accessor norm)
  (uv :vec2 :accessor uv))

(defun-g full-vert ((vert pos-norm-tex)
                   &uniform (model-to-clip :mat4))
  (values (* model-to-clip (v! (pos vert) 1))
          (uv vert)))

(defun-g full-frag ((uv :vec2) &uniform (sampler :sampler-2d))
  (texture sampler uv))

;;; Mandelbrot

(defun-g mandelbrot-color ((n :int) (iterations :int) (blackwhite :bool)
                           (color :vec3))
  (cond
    ((= n -1) (v! 0 0 0 1))
    (blackwhite (v! 0.7 0.7 0.7 1))
    ((< iterations 16)
     (let* ((r (int (+ (* (/ (1- n) (float (1- iterations))) 255)
                       0.5)))
            (g (- 255 r))
            (b (/ (* (mod (1- n) (/ iterations 2)) 255)
                  (/ iterations 2))))
       (v! (float (/ r 255.0))
           (float (/ g 255.0))
           (float (/ b 255.0))
           1)))
    (t
     (let* ((lim (min iterations 32))
            (r (/ (* (1- n) (x color)) lim))
            (g (/ (* (mod (1- n) (/ lim 4)) (y color)) (/ lim 4)))
            (b (/ (* (mod (1- n) (/ lim 8)) (z color)) (/ lim 8))))
       (v! (float (/ r 255.0))
           (float (/ g 255.0))
           (float (/ b 255.0))
           1)))))

(defmacro define-mandelbrot-shader (name (c z) &body body)
  (let ((c (alexandria:ensure-list c))
        (z (alexandria:ensure-list z)))
    (destructuring-bind ((c &optional (ic :pos))
                         (z &optional (iz '(v! 0 0))))
        (list c z)
      (labels ((initializer-expr (initializer)
                 ;; NOTE: Assumes POS is bound within the expansion
                 (if (eq initializer :pos) 'pos initializer)))
        (alexandria:with-gensyms (helper)
          `(defun-g ,name (&uniform (window :vec4) (viewport :vec2)
                                    (iterations :int) (blackwhite :bool)
                                    (color :vec3))
             (labels ((,helper ((,c :vec2) (,z :vec2))
                        ;; NOTE: The body can capture the GPU function's
                        ;; arguments
                        ,@body))
               (let ((pos (v! (mix (x window) (y window)
                                   (/ (x gl-frag-coord) (x viewport)))
                              (mix (z window) (w window)
                                   (/ (y gl-frag-coord) (y viewport))))))
                 (let ((c ,(initializer-expr ic))
                       (z ,(initializer-expr iz))
                       (n -1))
                   (dotimes (i iterations)
                     (setf z (,helper c z))
                     (let ((m (+ (* (x z) (x z))
                                 (* (y z) (y z)))))
                       (when (> m 4)
                         (setf n i)
                         (break))))
                   (mandelbrot-color n iterations blackwhite color))))))))))

(define-mandelbrot-shader mandelbrot-quadratic (c z)
  (v! (+ (- (* (x z) (x z))
            (* (y z) (y z)))
         (x c))
      (+ (* 2 (x z) (y z)) (y c))))

(define-mandelbrot-shader mandelbrot-cubic (c z)
  (v! (+ (- (* (x z) (x z) (x z))
            (* 3 (x z) (y z) (y z)))
         (x c))
      (+ (- (* 3 (x z) (x z) (y z))
            (* (y z) (y z) (y z)))
         (y c))))

(define-mandelbrot-shader julia ((c (v! -0.5 0.7)) (z :pos))
  (v! (+ (- (* (x z) (x z))
            (* (y z) (y z)))
         (x c))
      (+ (* 2 (x z) (y z)) (y c))))

;;; Kernels

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun calc-edge-len (weights)
    (multiple-value-bind (edge-len remainder) (floor (sqrt (length weights)))
      (assert (zerop remainder))
      (assert (oddp edge-len))
      edge-len)))

(defmacro define-kernel (name (&key normalize) &body weights)
  (let* ((edge-len (calc-edge-len weights))
         (offset (/ (- edge-len 1) 2))
         (weight-sum (reduce #'+ weights))
         (weight-index 0)
         (samples
           (loop :for y :from (- offset) :to offset
                 :append
                 (loop :for x :from (- offset) :to offset
                       :collect (let ((weight (elt weights weight-index)))
                                  `(* (texture tex (+ (* (v! ,x ,y) step) uv))
                                      ,weight))
                       :do (incf weight-index))))
         (body (if normalize
                   `(/ (+ ,@samples) ,weight-sum)
                   `(+ ,@samples))))
    `(defun-g ,name ((tex :sampler-2d) (uv :vec2) (step :vec2))
       ,body)))

(define-kernel k-id ()
  0 0 0
  0 1 0
  0 0 0)

(define-kernel k-edge ()
  -1 -1 -1
  -1  8 -1
  -1 -1 -1)

(define-kernel k-sharpen ()
  0  -1  0
  -1  5 -1
  0  -1  0)

(define-kernel k-gaussian (:normalize t)
  1  4  6  4 1
  4 16 24 16 4
  6 24 36 24 6
  4 16 24 16 4
  1  4  6  4 1)

;;; Background

(defun-g background-frag ((uv :vec2) &uniform (sampler :sampler-2d))
  (k-id sampler uv (v! 1 1))
  #+nil
  (k-edge sampler uv (v! 0.999 0.999))
  #+nil
  (k-sharpen sampler uv (v! 0.999 0.999))
  #+nil
  (k-gaussian sampler uv (v! 0.995 0.995)))

;;; Cubemap

(defun-g cube-vert ((vert pos-norm-tex) &uniform (model-to-clip :mat4))
  (values (* model-to-clip (v! (pos vert) 1))
          (pos vert)))

(defun-g cube-frag ((pos :vec3) &uniform (sampler :sampler-cube))
  (texture sampler pos))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Pipelines

(defpipeline-g texture-1 ()
  :vertex (full-vert pos-norm-tex)
  :fragment (mandelbrot-quadratic))

(defpipeline-g background ()
  :vertex (full-vert pos-norm-tex)
  :fragment (background-frag :vec2))

(defpipeline-g texture-2 ()
  :vertex (full-vert pos-norm-tex)
  :fragment (mandelbrot-cubic))

(defpipeline-g cube-1 ()
  :vertex (full-vert pos-norm-tex)
  :fragment (julia))

(defpipeline-g cube-2 ()
  :vertex (full-vert pos-norm-tex)
  :fragment (full-frag :vec2))

(defpipeline-g texture-3 ()
  :vertex (full-vert pos-norm-tex)
  :fragment (mandelbrot-quadratic))

(defpipeline-g cubemap ()
  :vertex (cube-vert pos-norm-tex)
  :fragment (cube-frag :vec3))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Streams

(defun make-square-stream ()
  (let ((gverts (make-gpu-array
                 (list (list (v! 1 1 0)
                             (v! 0 0 0)
                             (v! 1 1))
                       (list (v! -1 1 0)
                             (v! 0 0 0)
                             (v! 0 1))
                       (list (v! -1 -1 0)
                             (v! 0 0 0)
                             (v! 0 0))
                       (list (v! 1 -1 0)
                             (v! 0 0 0)
                             (v! 1 0)))
                 :element-type 'pos-norm-tex))
        (gindex (make-gpu-array
                 (list 0 1 2
                       2 3 0)
                 :element-type :ushort)))
    (make-buffer-stream gverts :index-array gindex)))

(defun make-dendrite-stream (dendrite-func &key (swap t) normals tex-coords
                                             (element-type :vec3))
  (destructuring-bind (verts indices)
      (funcall dendrite-func :normals normals :tex-coords tex-coords)
    (let ((gverts (make-gpu-array verts :element-type element-type))
          (gindex (make-gpu-array
                   (if swap 
                       (dendrite.primitives:swap-winding-order indices)
                       indices)
                   :element-type :ushort)))
      (make-buffer-stream gverts :index-array gindex))))

(defun make-cube-stream (&key normals tex-coords (element-type :vec3))
  (make-dendrite-stream #'dendrite.primitives:cube-data
                        :normals normals
                        :tex-coords tex-coords
                        :element-type element-type))

(defun make-box-stream (&key normals tex-coords (element-type :vec3))
  (make-dendrite-stream #'dendrite.primitives:box-data
                        :swap nil
                        :normals normals
                        :tex-coords tex-coords
                        :element-type element-type))

(defun load-cubemap-texture (paths &key element-type)
  (with-c-arrays-freed (ca (mapcar (lambda (p)
                                     (dirt:load-image-to-c-array p))
                                   paths))
    (make-cubemap-texture ca :element-type element-type)))

(defun make-cubemap-texture (obj &key element-type)
  (typecase obj
    (list (make-texture obj :element-type element-type :cubes t))
    (t (make-texture (make-list 6 :initial-element obj)
                     :element-type element-type :cubes t))))

(defun pull-g-array (obj)
  (let* ((data (pull-g obj))
         (m (length data))
         (n (length (car data)))
         (element-type (type-of (caar data)))
         (arr (make-array (list m n) :element-type element-type)))
    (loop :for a :in data
          :for i :from 0 :below m :do
            (loop :for b :in a
                  :for j :from 0 :below n :do
                    (setf (aref arr i j) b)))
    arr))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Demo

(defun step-demo ()
  (step-host)
  (update-repl-link)
  (clear)
  ;; Cubemap
  #+nil
  (map-g #'cubemap *box-stream*
         :model-to-clip (m4:* (cam->clip *camera*)
                              (world->cam *camera*)
                              (m4:translation (v! 0 0 0))
                              (m4:scale (v3! 1000)))
         :sampler (sample *cubemap*))
  ;; Texture 1
  (with-fbo-bound (*texture-1-fbo*)
    (clear-fbo *texture-1-fbo*)
    (map-g #'texture-1 *square-stream*
           :model-to-clip (m4:identity)
           :window (v! -1.7 0.5 -1 1)
           :viewport (viewport-size)
           :iterations 1000
           :blackwhite 0
           :color (v! 300 20 300)))
  ;; Background
  #+nil
  (map-g #'background *square-stream*
         :model-to-clip (m4:identity)
         :sampler (sample (gpu-array-texture
                           (attachment *texture-1-fbo* 0))))
  ;; Cube 1
  (map-g #'cube-1 *cube-stream*
         :model-to-clip
         (m4:* (cam->clip *camera*)
               (world->cam *camera*)
               (m4:translation (v! (cos (* 2 (now)))
                                   (* 3 (sin (* 2 (now))))
                                   2))
               (m4:translation (v! -2 0 1.5))
               (m4:rotation-from-euler
                (v! (sin (now)) (mod (now) 10000) 0))
               (m4:scale (v3! 2)))
         :window (v! -1.6 1.6 -1.3 1.4)
         :viewport (viewport-size)
         :iterations 1000
         :blackwhite 0
         :color (v! 200 10 100))
  ;; Texture 2
  (with-fbo-bound (*texture-2-fbo*)
    (clear-fbo *texture-2-fbo*)
    (map-g #'texture-2 *square-stream*
           :model-to-clip (m4:identity)
           :window (v! -1 1 -1 1)
           :viewport (viewport-size)
           :iterations 1000
           :blackwhite 0
           :color (v! 1000 450 1)))
  (loop :for i :from 0 :below 2 :do
    ;; Cube 2
    (map-g #'cube-2 *cube-stream*
           :model-to-clip (m4:* (cam->clip *camera*)
                                (world->cam *camera*)
                                (m4:translation
                                 (choose
                                  i
                                  (v! (cos (now))
                                      (* 3 (sin (* 2 (now))))
                                      4)
                                  (v! (cos (now))
                                      (* 3 (cos (* 2 (now))))
                                      4)))
                                (m4:translation (v! (choose i 2 6) 0 1.5))
                                (m4:rotation-from-euler
                                 (choose
                                  i
                                  (v! (* 6 (sin (now))) (mod (now) 10000) 0)
                                  (v! 0 (mod (now) 10000) (* 6 (sin (now))))))
                                (m4:scale (choose i (v3! 2) (v3! 1))))
           :sampler
           (sample (gpu-array-texture
                    (attachment *texture-2-fbo* 0)))))
  (swap))

(defun mouse-callback (moved &rest ignored)
  (declare (ignore ignored))
  (setf *mouse-ang* (v2:+ (v! (/ (v:x moved) -1500.0)
                              (/ (v:y moved) -1500.0))
                          *mouse-ang*)
        (dir *camera*) (v! (sin (v:x *mouse-ang*))
                           (sin (v:y *mouse-ang*))
                           (cos (v:x *mouse-ang*)))))

(defun update-cubemap-2 ()
  (let ((dim (apply #'min (viewport-dimensions (current-viewport)))))
    (with-viewport (make-viewport (list dim dim))
      (with-fbo-bound (*texture-3-fbo*)
        (clear-fbo *texture-3-fbo*)
        (map-g #'texture-3 *square-stream*
               :model-to-clip (m4:identity)
               :window (v! -1.7 0.5 -1 1)
               :viewport (viewport-size)
               :iterations 1000
               :blackwhite 1
               :color (v! 300 20 300))))
    (setf *cubemap-2* (make-cubemap-texture
                       (pull-g-array (attachment *texture-3-fbo* 0))
                       :element-type :rgba8)
          *cubemap* *cubemap-2*)))

(defun run-loop ()
  (let ((dim (apply #'min (viewport-dimensions (current-viewport)))))
    ;; Initialize
    (setf *running* t
          *camera* (make-camera -1.0 200.0 90.0)
          *square-stream* (make-square-stream)
          *cube-stream* (make-cube-stream
                         :normals t
                         :tex-coords t
                         :element-type 'pos-norm-tex)
          *box-stream* (make-box-stream
                        :normals t
                        :tex-coords t
                        :element-type 'pos-norm-tex)
          *texture-1-fbo* (make-fbo 0 :d)
          *texture-2-fbo* (make-fbo 0 :d)
          *texture-3-fbo* (make-fbo `(0 :dimensions (,dim ,dim))
                                    `(:d :dimensions (,dim ,dim)))
          *cubemap-1* (load-cubemap-texture
                       (mapcar
                        (lambda (p) (merge-pathnames p *dir*))
                        '("cubemap/left.png"
                          "cubemap/right.png"
                          "cubemap/up.png"
                          "cubemap/down.png"
                          "cubemap/front.png"
                          "cubemap/back.png"))
                       :element-type :rgb8)
          *cubemap* *cubemap-1*)
    (update-cubemap-2))
  (loop :while (and *running* (not (shutting-down-p))) :do
    (whilst-listening-to ((#'mouse-callback (mouse 0) :move))
      (continuable (step-demo)))))

(defun stop-loop ()
  (setf *running* nil))

(defun main ()
  (repl)
  (setf (window-title) "RG Lab3"
        (window-size) (list (/ 1920 2) 1080))
  (run-loop))

(in-package #:rg-lab3)

(defmacro choose (index &rest exprs)
  `(ecase ,index
     ,@(mapcar (lambda (i expr)
                 `(,i ,expr))
        (loop :for i :from 0 :below (length exprs) :collect i)
        exprs)))

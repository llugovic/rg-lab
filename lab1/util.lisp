(in-package #:rg-lab1.exercise1)

(defun sym (&rest things)
  (intern
   (string-upcase
    (with-output-to-string (s)
      (dolist (th things)
        (princ th s))))))

(defmacro string-case (keyform &body clauses)
  (alexandria:once-only (keyform)
    `(cond
       ,@(mapcar (lambda (c)
                   (let ((test (car c))
                         (body (cdr c)))
                     (if (eq test t)
                         `(t ,@body)
                         `((string-equal ,keyform ,test) ,@body))))
                 clauses))))

(defmacro wrap-case1 (keyform &body clauses)
  (labels ((is-before-clause-p (clause)
             (eq (car clause) 'before))
           (is-after-clause-p (clause)
             (eq (car clause) 'after))
           (is-normal-clause-p (clause)
             (and (not (is-before-clause-p clause))
                  (not (is-after-clause-p clause)))))
    (let* ((before-clause (find-if #'is-before-clause-p clauses))
           (after-clause (find-if #'is-after-clause-p clauses))
           (clauses (remove-if-not #'is-normal-clause-p clauses)))
      (alexandria:once-only (keyform)
        `(progn
           ,@(when before-clause
               `((case ,keyform
                   (,(mapcar #'car clauses) ,@(cdr before-clause)))))
           (case ,keyform
             ,@clauses)
           ,@(when after-clause
               `((case ,keyform
                   (,(mapcar #'car clauses) ,@(cdr after-clause))))))))))

(defmacro wrap-case (keyform &body clauses)
  (labels ((is-before-clause-p (clause)
             (eq (car clause) 'before))
           (is-after-clause-p (clause)
             (eq (car clause) 'after))
           (is-normal-clause-p (clause)
             (and (not (is-before-clause-p clause))
                  (not (is-after-clause-p clause)))))
    (let* ((before-clause (find-if #'is-before-clause-p clauses))
           (after-clause (find-if #'is-after-clause-p clauses))
           (clauses (remove-if-not #'is-normal-clause-p clauses)))
      (alexandria:with-gensyms (before after)
        (alexandria:once-only (keyform)
          `(labels ((,before ()
                      ,@(cdr before-clause))
                    (,after ()
                      ,@(cdr after-clause)))
             (case ,keyform
               ,@(mapcar (lambda (c)
                           (list (car c) `(progn (,before) ,@(cdr c) (,after))))
                  clauses))))))))

(defmacro continuable (&body body)
  `(restart-case
       (progn ,@body) (continue () :report "Continue")))

(defun update-swank ()
  (continuable
    (let ((connection
            (or swank::*emacs-connection*
                (swank::default-connection))))
      (when connection
        (swank::handle-requests connection t)))))


(in-package #:rg-lab1.exercise1)

(defmacro with-filled-gl-array ((arr-sym type contents) &body body)
  (let ((contents-sym (gensym "CONTENTS"))
        (count-sym (gensym "COUNT")))
    `(let* ((,contents-sym ,contents)
            (,count-sym (length ,contents-sym)))
       (gl:with-gl-array (,arr-sym ,type :count ,count-sym)
         (dotimes (i ,count-sym)
           (setf (gl:glaref ,arr-sym i)
                 (aref ,contents-sym i)))
         ,@body))))

(defun upload-buffer-data (target type hint data)
  (with-filled-gl-array (arr type data)
    (gl:buffer-data target hint arr)))

(defun vector-push-extend-vec4 (vec4 vector)
  (with-vec4 (x y z w) vec4
    (vector-push-extend x vector)
    (vector-push-extend y vector)
    (vector-push-extend z vector)
    (vector-push-extend w vector)))

(defun vec3->array (v)
  (let ((array (make-array 3)))
    (setf (aref array 0) (vx v)
          (aref array 1) (vy v)
          (aref array 2) (vz v))
    array))

(defun vec4->array (v)
  (let ((array (make-array 4)))
    (setf (aref array 0) (vx v)
          (aref array 1) (vy v)
          (aref array 2) (vz v)
          (aref array 3) (vw v))
    array))

(defun vec2->vec3 (v)
  (vec (vx v) (vy v) 1.0))

(defun vec3->vec4 (v)
  (vec (vx v) (vy v) (vz v) 1.0))

(defun vec3-normalize (v)
  (with-vec3 (x y w) v
    (vec3 (/ x w) (/ y w) 1.0)))

(defun vec4-normalize (v)
  (with-vec4 (x y z w) v
    (vec4 (/ x w) (/ y w) (/ z w) 1.0)))

(defun vec4->vec3 (v)
  (vxyz (vec4-normalize v)))

(defun vec3->vec2 (v)
  (vxy (vec3-normalize v)))

(defun look-at (eye target up)
  (let* ((z (nvunit (v- eye target)))
         (x (nvunit (vc up z)))
         (y (vc z x)))
    (mat (vx3 x) (vy3 x) (vz3 x) (- (v. x eye))
         (vx3 y) (vy3 y) (vz3 y) (- (v. y eye))
         (vx3 z) (vy3 z) (vz3 z) (- (v. z eye))
         0 0 0 1)))

(in-package #:rg-lab1.exercise1)

(defparameter *width* 640)
(defparameter *height* 480)

(defparameter *vert-shader-src*
  (alexandria:read-file-into-string
   (asdf:system-relative-pathname :rg-lab1 "phong.vs")))

(defparameter *frag-shader-src*
  "#version 330 core

in vec4 frag_color;

void main()
{
    gl_FragColor = frag_color;
}")

(defparameter *vao* nil)
(defparameter *vbo* nil)
(defparameter *vao-spline* nil)
(defparameter *vbo-spline* nil)
(defparameter *program* nil)
(defparameter *vert-shader* nil)
(defparameter *frag-shader* nil)

(defparameter *uniform-model-mat* nil)
(defparameter *uniform-view-mat* nil)
(defparameter *uniform-proj-mat* nil)
(defparameter *uniform-light* nil)
(defparameter *uniform-i-ambient* nil)
(defparameter *uniform-i-diffuse* nil)
(defparameter *uniform-i-specular* nil)
(defparameter *uniform-k-ambient* nil)
(defparameter *uniform-k-diffuse* nil)
(defparameter *uniform-k-specular* nil)
(defparameter *uniform-shininess* nil)

(defparameter *current-model* nil)
(defparameter *camera-eye* nil)
(defparameter *camera-target* nil)
(defparameter *camera-up* nil)
(defparameter *camera-distance* nil)
(defparameter *camera-param* nil)
(defparameter *camera-param-delta* (* 5 (/ pi 180)))

(defparameter *proj-half-width* 0.5)
(defparameter *proj-half-height* 0.5)
(defparameter *proj-z-near* 1)
(defparameter *proj-z-far* 100)

(defparameter *model-mat* (meye 4))
(defparameter *view-mat* nil)
(defparameter *proj-mat* (mfrustum (- *proj-half-width*) *proj-half-width*
                                   (- *proj-half-height*) *proj-half-height*
                                   *proj-z-near* *proj-z-far*))

(defparameter *shading* 'flat)
(defparameter *light* (vec4 4 5 3 1))
(defparameter *i-ambient* (vec3 0.2 0.2 0.2))
(defparameter *i-diffuse* (vec3 0.8 0.8 0))
(defparameter *i-specular* (vec3 0.1 1 0.1))
(defparameter *k-ambient* (vec3 1 1 1))
(defparameter *k-diffuse* (vec3 1 1 1))
(defparameter *k-specular* (vec3 0.01 0.01 0.01))
(defparameter *shininess* 96)

(defparameter *z-buffering* t)
(defparameter *polygon-mode-line* t)
(defparameter *cull-mode* 'opengl)
(defparameter *spline*
  (make-cubic-b-spline
   (mapcar (lambda (v) (apply #'vec4 v))
           '((0 0 0 1) (0 10 5 1) (10 10 10 1) (10 0 15 1)
             (0 0 20 1) (0 10 25 1) (10 10 30 1) (10 0 35 1)
             (0 0 40 1) (0 10 45 1) (10 10 50 1) (10 0 55 1)))))
(defparameter *spline-lines* nil)
(defparameter *spline-param* 0)
(defparameter *spline-segment* 0)

(defun setup-opengl ()
  (setf *vao* (gl:gen-vertex-array)
        *vao-spline* (gl:gen-vertex-array)
        *vbo* (gl:gen-buffer)
        *vbo-spline* (gl:gen-buffer))

  (setf *program* (gl:create-program)
        *vert-shader* (gl:create-shader :vertex-shader)
        *frag-shader* (gl:create-shader :fragment-shader))

  (gl:shader-source *vert-shader* *vert-shader-src*)
  (gl:shader-source *frag-shader* *frag-shader-src*)

  (gl:bind-vertex-array *vao*)
  (gl:enable-vertex-attrib-array 0)
  (gl:enable-vertex-attrib-array 1)

  (gl:bind-vertex-array *vao-spline*)
  (gl:enable-vertex-attrib-array 0)

  (gl:compile-shader *vert-shader*)
  (gl:compile-shader *frag-shader*)

  (unless (gl:get-shader *vert-shader* :compile-status)
    (error (format nil "Compilation failed for vert-shader:~%~a~%"
                   (gl:get-shader-info-log *vert-shader*))))
  (unless (gl:get-shader *frag-shader* :compile-status)
    (error (format nil "Compilation failed for vert-shader:~%~a~%"
                   (gl:get-shader-info-log *frag-shader*))))

  (gl:attach-shader *program* *vert-shader*)
  (gl:attach-shader *program* *frag-shader*)
  (gl:link-program *program*)

  (unless (gl:get-program *program* :link-status)
    (error (format nil "Linking failed for program:~%~a~%"
                   (gl:get-program-info-log *program*))))

  (gl:use-program *program*)

  (setf *uniform-model-mat* (gl:get-uniform-location *program* "model_mat")
        *uniform-view-mat* (gl:get-uniform-location *program* "view_mat")
        *uniform-proj-mat* (gl:get-uniform-location *program* "proj_mat")
        *uniform-light* (gl:get-uniform-location *program* "light")
        *uniform-i-ambient* (gl:get-uniform-location *program* "i_ambient")
        *uniform-i-diffuse* (gl:get-uniform-location *program* "i_diffuse")
        *uniform-i-specular* (gl:get-uniform-location *program* "i_specular")
        *uniform-k-ambient* (gl:get-uniform-location *program* "k_ambient")
        *uniform-k-diffuse* (gl:get-uniform-location *program* "k_diffuse")
        *uniform-k-specular* (gl:get-uniform-location *program* "k_specular")
        *uniform-shininess* (gl:get-uniform-location *program* "shininess"))

  (when *z-buffering*
    (gl:enable :depth-test))

  (when *polygon-mode-line*
    (gl:polygon-mode :front-and-back :line))

  (gl:cull-face :back))

(defun tear-opengl ()
  (gl:delete-vertex-arrays (list *vao* *vao-spline*))
  (gl:delete-buffers (list *vbo* *vbo-spline*))
  (gl:delete-shader *vert-shader*)
  (gl:delete-shader *frag-shader*)
  (gl:delete-program *program*))

(defun shader-send-mats ()
  (gl:uniform-matrix-4fv *uniform-model-mat* (marr4 *model-mat*))
  (gl:uniform-matrix-4fv *uniform-view-mat* (marr4 *view-mat*))
  (gl:uniform-matrix-4fv *uniform-proj-mat* (marr4 *proj-mat*)))

(defun shader-send-light ()
  (gl:uniformfv *uniform-light* (vec4->array *light*))
  (gl:uniformfv *uniform-i-ambient* (vec3->array *i-ambient*))
  (gl:uniformfv *uniform-i-diffuse* (vec3->array *i-diffuse*))
  (gl:uniformfv *uniform-i-specular* (vec3->array *i-specular*))
  (gl:uniformfv *uniform-k-ambient* (vec3->array *k-ambient*))
  (gl:uniformfv *uniform-k-diffuse* (vec3->array *k-diffuse*))
  (gl:uniformfv *uniform-k-specular* (vec3->array *k-specular*))
  (gl:uniformf *uniform-shininess* *shininess*))

(defun reset-camera ()
  (setf *camera-eye* (vec3 3 0 1)
        *camera-target* (vec3 0 0 0)
        *camera-up* (vec3 0 1 0)
        *camera-param* (atan 1/3)
        *camera-distance* (/ 1 (sin *camera-param*)))

  (setf *view-mat* (mlookat *camera-eye* *camera-target* *camera-up*))
  (shader-send-mats))

(defun keydown (keysym)
  (let ((scancode (sdl2:scancode keysym)))
    (wrap-case scancode
      (:scancode-h (incf *camera-param* *camera-param-delta*))
      (:scancode-l (incf *camera-param* (- *camera-param-delta*)))
      (:scancode-j (incf *camera-distance* *camera-param-delta*))
      (:scancode-k (incf *camera-distance* (- *camera-param-delta*)))
      (:scancode-r (reset-camera))
      (after
       (setf *camera-eye* (vec3 (* *camera-distance* (cos *camera-param*))
                                0
                                (* *camera-distance* (sin *camera-param*))))
       (setf *view-mat* (mlookat *camera-eye* *camera-target* *camera-up*))
       (shader-send-mats)))

    (case scancode
      (:scancode-s
       (setf *shading* (if (eq *shading* 'flat)
                           'gouraud
                           'flat))
       (upload-model *current-model*))
      (:scancode-y
       (setf *z-buffering* (not *z-buffering*))
       (if *z-buffering*
           (gl:enable :depth-test)
           (gl:disable :depth-test)))
      (:scancode-p
       (setf *polygon-mode-line* (not *polygon-mode-line*))
       (if *polygon-mode-line*
           (gl:polygon-mode :front-and-back :line)
           (gl:polygon-mode :front-and-back :fill))))

    (wrap-case scancode
      (:scancode-0 (setf *cull-mode* nil))
      (:scancode-1 (setf *cull-mode* 'algorithm1))
      (:scancode-2 (setf *cull-mode* 'algorithm2))
      (:scancode-3 (setf *cull-mode* 'algorithm3))
      (:scancode-4 (setf *cull-mode* 'opengl))
      (after
       (format t "Cull mode: ~a~%" *cull-mode*)
       (if (eq *cull-mode* 'opengl)
           (gl:enable :cull-face)
           (gl:disable :cull-face))
       (upload-model *current-model*)))))

(defun reshape (event data1 data2)
  ;; https://wiki.libsdl.org/SDL_WindowEventID
  ;; WindowEvent enumerations start at 0
  (when (= event 5)
    (setf *width* data1
          *height* data2))
  (when (or (= event 1) (= event 5))
    (let* ((min (min *width* *height*))
           (x (- (/ *width* 2) (/ min 2)))
           (y (- (/ *height* 2) (/ min 2))))
      (gl:viewport x y min min))))

(defun display (win model &optional spline)
  (gl:clear-color 1 1 1 1)
  (gl:clear :color-buffer :depth-buffer)

  (shader-send-mats)
  (gl:bind-vertex-array *vao-spline*)
  (gl:draw-arrays :lines 0 *spline-lines*)

  (when spline
    (let* ((params (list *spline* *spline-segment* *spline-param*))
           (vp (apply #'cubic-b-spline-point params))
           (v1d (apply #'cubic-b-spline-deriv1 params))
           (v2d (apply #'cubic-b-spline-deriv2 params))
           (o vp)
           (w (vunit v1d))
           (u (vunit (vec3->vec4 (vc (vxyz v1d) (vxyz v2d)))))
           (v (vec3->vec4 (vc (vxyz w) (vxyz u))))
           (*model-mat*
             (m* (mtranslation o)
                 (mat4 (list
                        (vx w) (vx u) (vx v) 0
                        (vy w) (vy u) (vy v) 0
                        (vz w) (vz u) (vz v) 0
                        0 0 0 1)))))
      (shader-send-mats)
      (gl:bind-vertex-array *vao*)
      (gl:draw-arrays :triangles 0 (* 3 (length (triangles model))))
      (update-spline-param)))

  (sdl2:gl-swap-window win))

(defun update-spline-param ()
  (incf *spline-param* 0.01)
  (when (> *spline-param* 1)
    (incf *spline-segment*)
    (when (>= *spline-segment* (length (segments *spline*)))
      (setf *spline-segment* 0))
    (setf *spline-param* 0)))

(defun idle (win model &optional spline)
  #+swank
  (update-swank)
  (display win model spline))

(defun napply-transform (point)
  (m* *proj-mat* *view-mat* point))

(defun point-gouraud-normal (model point-index)
  (let ((triangles)
        (norm (vec4 0 0 0 0)))
    (loop :for i :from 0
          :for tri :across (triangles model) :do
            (when (find point-index tri)
              (push i triangles)))
    (loop :for triangle-index :in triangles :do
      (nv+ norm (vxyz_ (aref (planes model) triangle-index))))
    (v/ norm (length triangles))))

(defun upload-model (model)
  (gl:bind-vertex-array *vao*)
  (gl:bind-buffer :array-buffer *vbo*)

  (let ((len (length (triangles model)))
        (normals (make-array (length (vertices model))))
        (data (make-array 0 :element-type 'float :adjustable t :fill-pointer 0)))
    (loop :for i :from 0 :below len
          :for tr = (get-triangle model i) :do
            (cond ((or (null *cull-mode*)
                       (eq *cull-mode* 'opengl)
                       (and (eq *cull-mode* 'algorithm1)
                            (is-visible1 (aref (planes model) i) *camera-eye*))
                       (and (eq *cull-mode* 'algorithm2)
                            (is-visible2 tr (aref (planes model) i) *camera-eye*)))
                   (loop :for u :across tr
                         :for v = (vec3->vec4 u)
                         :do (vector-push-extend-vec4 v data)))
                  ((eq *cull-mode* 'algorithm3)
                   (let ((proj-tr (map 'vector
                                       (lambda (v)
                                         (vec4->vec3 (napply-transform (vec3->vec4 v))))
                                       tr)))
                     (when (is-visible3 proj-tr)
                       (loop :for u :across tr
                             :for v = (vec3->vec4 u)
                             :do (vector-push-extend-vec4 v data)))))))
    (when (eq *shading* 'gouraud)
      (loop :for point-index :from 0 :below (length (vertices model)) :do
        (setf (aref normals point-index)
              (point-gouraud-normal model point-index))))
    (loop :for i :from 0 :below len :do
      (case *shading*
        (flat (let ((norm (vxyz_ (aref (planes model) i))))
                (dotimes (_ 3)
                  (vector-push-extend-vec4 norm data))))
        (gouraud (let ((tri (aref (triangles model) i)))
                   (loop :for point-index :across tri :do
                     (vector-push-extend-vec4 (aref normals point-index) data))))))

    (upload-buffer-data :array-buffer :float :static-draw data)
    (gl:vertex-attrib-pointer 0 4 :float :false 0 0)
    (gl:vertex-attrib-pointer 1 4 :float :false 0 (* len 3 4 4))))

(defun upload-spline (spline)
  (gl:bind-vertex-array *vao-spline*)
  (gl:bind-buffer :array-buffer *vbo-spline*)

  (let ((data (make-array 0 :element-type 'float :adjustable t :fill-pointer 0))
        (current nil)
        (next nil))
    (do-cubic-spline (v) *spline*
      (setf current next
            next v)
      (when current
        (vector-push-extend-vec4 current data)
        (vector-push-extend-vec4 next data)))

    (setf *spline-lines* (/ (length data) 2))
    (upload-buffer-data :array-buffer :float :static-draw data)
    (gl:vertex-attrib-pointer 0 4 :float :false 0 0)))

(defun draw-model (model)
  (sdl2:make-this-thread-main
   (lambda ()
     (let ((*current-model* model))
       (sdl2:with-init (:everything)
         (sdl2:with-window (win :title "Models" :flags '(:opengl)
                                :w *width* :h *height*)
           (sdl2:gl-set-attr :context-major-version 3)
           (sdl2:gl-set-attr :context-minor-version 3)

           (sdl2:with-gl-context (gl win)
             (sdl2:gl-make-current win gl)

             (setup-opengl)
             (reset-camera)
             (shader-send-light)
             (upload-model model)
             (upload-spline *spline*)

             (sdl2:with-event-loop (:method :poll)
               (:quit () t)
               (:keydown (:keysym keysym) (keydown keysym))
               (:windowevent (:event event :data1 data1 :data2 data2) (reshape event data1 data2))
               (:idle () (idle win model *spline*)))

             (tear-opengl))))))))

(in-package #:rg-lab1.exercise1)

(defun is-visible1 (plane eye)
  (> (v. plane eye) 0))

(defun is-visible2 (triangle plane eye)
  (let* ((v1 (aref triangle 0))
         (v2 (aref triangle 1))
         (v3 (aref triangle 2))
         (c (v/ (v+ v1 v2 v3) 3))
         (e (v- eye c))
         (n (vxyz plane)))
    (> (v. n e) 0)))

(defun compute-directions (vertices)
  (let ((len (length vertices)))
    (loop :for i :from 0 :below len
          :for t1 = (aref vertices i)
          :for t2 = (aref vertices (mod (+ i 1) len))
          :for t3 = (aref vertices (mod (+ i 2) len))
          :collect (v. t3 (vc t1 t2)))))

(defun is-visible3 (triangle)
  (let ((len (length triangle))
        (directions (compute-directions triangle))
        (count 0))
    (loop :for d :in directions
          :while (> d 0)
          :do (incf count))
    (= count len)))

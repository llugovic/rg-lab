(in-package #:rg-lab1.exercise1)

(defparameter *commands*
  '(("list") ("show") ("point") ("normalize") ("draw") ("quit")))

(defparameter *obj-directory*
  (asdf:system-relative-pathname :rg-lab "obj/"))

(defun model-to-path (name)
  (merge-pathnames *obj-directory* (format nil "~a.obj" name)))

(defun model-exists-p (name)
  (uiop:file-exists-p (model-to-path name)))

(defun ask-model ()
  (format t "Enter a model name: ")
  (let ((name (read-line)))
    (when (model-exists-p name)
      name)))

(defun list-command ()
  (format t "Available models:~%~{~a~%~}"
          (mapcar #'pathname-name
                  (uiop:directory-files *obj-directory* "*.obj"))))

(defun show-command ()
  (let ((name (ask-model)))
    (unless name
      (format t "Invalid model!~%")
      (return-from show-command))
    (write-model t (load-model name))))

(defun point-command ()
  (let ((name (ask-model)))
    (unless name
      (format t "Invalid model!~%")
      (return-from point-command))
    (format t "Enter a point: ")
    (let* ((parts (ppcre:split "," (read-line)))
           (nums (handler-case (map 'vector #'parse-float:parse-float parts)
                   (parse-error ()
                     (format t "Invalid input!~%")
                     (return-from point-command)))))
      (when (/= (length nums) 3)
        (format t "Incorrect number of coordinates!~%")
        (return-from point-command))
      (let ((vert (vec (aref nums 0) (aref nums 1) (aref nums 2))))
        (format t "The point is ~a the object!~%"
                (model-vs-point (load-model name) vert))))))

(defun normalize-command ()
  (let ((name (ask-model)))
    (unless name
      (format t "Invalid model!~%")
      (return-from normalize-command))
    (write-model t (normalize-model (load-model name)))))

(defun draw-command ()
  (let ((name (ask-model)))
    (unless name
      (format t "Invalid model!~%")
      (return-from draw-command))
    (draw-model (normalize-model (load-model name)))))

(defun invoke-command (cmd)
  (if (cdr cmd)
      (funcall (cdr cmd))
      (funcall (symbol-function (sym (car cmd) "-command")))))

(defun main ()
  (loop
    (format t "Enter a command (~{~a~^, ~}): "
            (mapcar #'car *commands*))
    (let ((cmd (assoc (read-line) *commands* :test #'equalp)))
      (if cmd
          (string-case (car cmd)
            ("quit" (progn (format t "Goodbye!") (return-from main)))
            (t (invoke-command cmd)))
          (format t "Invalid command!~%")))))

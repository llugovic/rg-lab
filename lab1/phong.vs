#version 330 core

uniform mat4 model_mat;
uniform mat4 view_mat;
uniform mat4 proj_mat;

uniform vec4 light;
uniform vec3 i_ambient;
uniform vec3 i_diffuse;
uniform vec3 i_specular;
uniform vec3 k_ambient;
uniform vec3 k_diffuse;
uniform vec3 k_specular;
uniform float shininess;

layout(location = 0) in vec4 pos;
layout(location = 1) in vec4 norm;

out vec4 frag_color;

vec4 phong_color(vec3 c_pos, vec3 c_norm, vec3 c_light)
{
    vec3 l = normalize(c_light - c_pos);
    vec3 r = reflect(-l, c_norm);
    vec3 v = normalize(-c_pos);

    vec3 ambient = k_ambient * i_ambient;

    float angle_diffuse = max(dot(l, c_norm), 0);
    vec3 diffuse = k_diffuse * i_diffuse * angle_diffuse;

    float angle_specular = max(dot(r, v), 0);
    vec3 specular = k_specular * i_specular * pow(angle_diffuse, shininess);

    return vec4(ambient + diffuse + specular, 1);
}

vec3 vec4_divide(vec4 v)
{
    return v.xyz / v.w;
}

void main()
{
    vec3 c_pos = vec4_divide(view_mat * pos);
    vec3 c_norm = normalize(vec4_divide(transpose(inverse(view_mat)) * norm));
    vec3 c_light = vec4_divide(view_mat * light);

    gl_Position = proj_mat * view_mat * model_mat * pos;
    frag_color = phong_color(c_pos, c_norm, c_light);
}

(in-package #:rg-lab1.exercise1)

(defclass cubic-b-spline ()
  ((points
    :initarg :points
    :initform (error "~s is a required initarg" :points)
    :reader points
    :accessor %points)
   (segments
    :reader segments
    :accessor %segments)))

(defmethod initialize-instance :after ((spline cubic-b-spline) &key)
  (with-accessors ((points %points)
                   (segments %segments))
      spline
    (unless (>= (length points) 4)
      (error "A spline must be defined by at least 4 points"))
    (setf points (apply #'vector points))
    (let ((n (length points)))
      (setf segments
            (loop :for i :from 0 :below (- n 3)
                  :for v1 = (elt points i)
                  :for v2 = (elt points (+ i 1))
                  :for v3 = (elt points (+ i 2))
                  :for v4 = (elt points (+ i 3))
                  :collect (mat4 (list
                                  (vx v1) (vx v2) (vx v3) (vx v4)
                                  (vy v1) (vy v2) (vy v3) (vy v4)
                                  (vz v1) (vz v2) (vz v3) (vz v4)
                                  (vw v1) (vw v2) (vw v3) (vw v4)))))
      (setf segments (apply #'vector segments)))))

(defun make-cubic-b-spline (points)
  (make-instance 'cubic-b-spline :points points))

(defparameter *cubic-b-spline-b1-matrix*
  (m* (/ 1 6)
      (mat4 '(-1 +3 -3 +1
              +3 -6 +0 +4
              -3 +3 +3 +1
              +1 +0 +0 +0)))
  "The matrix for calculating a cubic B-spline's point. Assumes a T vector of
the form [t^3 t^2 t 1].")

(defparameter *cubic-b-spline-b2-matrix*
  (m* (/ 1 2)
      (mat4 '(-1 +2 -1 +0
              +3 -4 +0 +0
              -3 +2 +1 +0
              +1 +0 +0 +0)))
  "The matrix for calculating a cubic B-spline's (first) derivative. Assumes a T
vector of the form [t^2 t 1 0].")

(defparameter *cubic-b-spline-b3-matrix*
  (m* (/ 1 2)
      (mat4 '(-1 +2 +0 +0
              +3 -4 +0 +0
              -3 +2 +0 +0
              +1 +0 +0 +0)))
  "The matrix for calculating a cubic B-spline's second derivative. Assumes a T
vector of the form [2t 1 0 0].")

(defun %cubic-b-spline-point (spline segment vparam)
  (m* (aref (segments spline) segment)
      *cubic-b-spline-b1-matrix*
      vparam))

(defun cubic-b-spline-point (spline segment param)
  (%cubic-b-spline-point
   spline
   segment
   (vec4 (* param param param) (* param param) param 1)))

(defun %cubic-b-spline-deriv1 (spline segment vparam)
  (m* (aref (segments spline) segment)
      *cubic-b-spline-b2-matrix*
      vparam))

(defun cubic-b-spline-deriv1 (spline segment param)
  (%cubic-b-spline-deriv1
   spline
   segment
   (vec4 (* param param) param 1 0)))

(defun %cubic-b-spline-deriv2 (spline segment vparam)
  (m* (aref (segments spline) segment)
      *cubic-b-spline-b3-matrix*
      vparam))

(defun cubic-b-spline-deriv2 (spline segment param)
  (%cubic-b-spline-deriv2
   spline
   segment
   (vec4 (* 2 param) 1 0 0)))

(defmacro do-cubic-spline ((vs &optional (step 0.01)) spline &body body)
  "Iterate over all of the points of a cubic B-spline SPLINE. SPLINE should be
an instance of CUBIC-B-SPLINE.

VS is either a symbol V or a list of the form (V VD1 VD2). V, VD1 and VD2 are
VEC4s bound to the following values on every iteration (i.e. for every change of
the parameter):

- V is a point on the curve.

- VD1 is the (first) derivative (tangent) of the curve in the point V.

- VD2 is the second derivative of the curve in the point V."
  (alexandria:once-only (spline)
    (let ((vs (alexandria:ensure-list vs))
          (segment (gensym "SEGMENT"))
          (param (gensym "T"))
          (vparam (gensym "VT")))
      (destructuring-bind (vp &optional vd1 vd2) vs
        `(loop :for ,segment :from 0 :below (length (segments spline)) :do
          (loop :named nil
                :for ,param :from 0 :below 1 :by ,step
                ,@(when (or vp vd1 vd2) `(:with ,vparam = (vec4 0 0 0 0)))
                ,@(when vp `(:with ,vp))
                ,@(when vd1 `(:with ,vd1))
                ,@(when vd2 `(:with ,vd2)) :do
                  (progn
                    ,@(when vp
                        `((setf (vx ,vparam) (* ,param ,param ,param))
                          (setf (vy ,vparam) (* ,param ,param))
                          (setf (vz ,vparam) ,param)
                          (setf (vw ,vparam) 1)
                          (setf ,vp
                                (%cubic-b-spline-point ,spline ,segment
                                                       ,vparam))))
                    ,@(when vd1
                        `((setf (vx ,vparam) (* ,param ,param))
                          (setf (vy ,vparam) ,param)
                          (setf (vz ,vparam) 1)
                          (setf (vw ,vparam) 0)
                          (setf ,vd1
                                (%cubic-b-spline-deriv1 ,spline ,segment
                                                        ,vparam))))
                    ,@(when vd2
                        `((setf (vx ,vparam) (* 2 ,param))
                          (setf (vy ,vparam) 1)
                          (setf (vz ,vparam) 0)
                          (setf (vw ,vparam) 0)
                          (setf ,vd2
                                (%cubic-b-spline-deriv1 ,spline ,segment
                                                        ,vparam))))
                    ,@body)))))))

(asdf:defsystem #:rg-lab1
  :description "RG Lab1"
  :author "Lovro Lugović <lovro.lugovic@fer.hr>"
  :license  "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on ("alexandria"
               "cl-opengl"
               "sdl2"
               "3d-vectors"
               "3d-matrices"
               "parse-float")
  :components ((:file "package")
               (:file "util")
               (:file "spline")
               (:file "draw-util")
               (:file "model")
               (:file "draw")
               (:file "cull")
               (:file "exercise1")))

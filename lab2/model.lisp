(in-package #:rg-lab2.exercise1)

(defclass model ()
  ((vertices :initarg :vertices :initform #() :accessor vertices)
   (triangles :initarg :triangles :initform #() :accessor triangles)
   (planes :accessor planes)))

(defmethod initialize-instance :after ((model model) &key)
  (setf (planes model) (calculate-planes model)))

(defun calculate-planes (model)
  (let* ((len (length (triangles model)))
         (arr (make-array len)))
    (loop :for i :from 0 :below len
          :for tr = (get-triangle model i)
          :for v0 = (aref tr 0)
          :for v1 = (aref tr 1)
          :for v2 = (aref tr 2)
          :for vx = (v- v1 v0)
          :for vy = (v- v2 v0)
          :for n = (vc vx vy)
          :for d = (- (v. n v0)) :do
            ;; TODO: same errors, Shinmera bug?
            ;; (setf (aref (normals model) i)
            ;;       (vc (v- (aref tr 1) (aref tr 0))
            ;;           (v- (aref tr 2) (aref tr 0))))
            (setf (aref arr i) (vec4 (vx n) (vy n) (vz n) d)))
    arr))

(defun get-triangle (model i)
  (let* ((vertices (vertices model))
         (triangles (triangles model))
         (triangle (aref triangles i)))
    (make-array 3 :initial-contents (list (aref vertices (aref triangle 0))
                                          (aref vertices (aref triangle 1))
                                          (aref vertices (aref triangle 2))))))

(defun model-vs-point (model vert)
  (let ((pos 0)
        (vert (vec3->vec4 vert)))
    (loop :for n :across (planes model)
          :for b = (v. n vert) :do
            (cond
              ((> b 0) (incf pos))
              ((= b 0) (return-from model-vs-point 'on))))
    (if (> pos 0)
        'outside
        'inside)))

(defun parse-vertex (strs)
  (apply #'vec (mapcar #'parse-float:parse-float strs)))

(defun parse-triangle (strs)
  (map 'vector (lambda (s) (1- (parse-integer s))) strs))

(defun load-model-file (path)
  (let ((vertices (make-array 0 :adjustable t :fill-pointer 0))
        (triangles (make-array 0 :adjustable t :fill-pointer 0)))
    (with-open-file (f path :direction :input :if-does-not-exist nil)
      (unless f
        (error "Couldn't open the file ~a!" path))
      (loop :for line = (read-line f nil)
            :while line
            :for str = (string-trim '(#\Space #\Return #\Linefeed #\Tab) line) :do
              (when (not (zerop (length str)))
                (let* ((parts (ppcre:split "\\s+" str))
                       (type (aref (car parts) 0)))
                  (case type
                    (#\v (vector-push-extend (parse-vertex (cdr parts)) vertices))
                    (#\f (vector-push-extend (parse-triangle (cdr parts)) triangles))))))
      (make-instance 'model :vertices vertices
                            :triangles triangles))))

(defun load-model (name)
  (load-model-file (model-to-path name)))

(defun normalize-model (model)
  (multiple-value-bind (xmin xmax ymin ymax zmin zmax)
      (loop :for v :across (vertices model)
            :minimize (vx v) :into xmin
            :maximize (vx v) :into xmax
            :minimize (vy v) :into ymin
            :maximize (vy v) :into ymax
            :minimize (vz v) :into zmin
            :maximize (vz v) :into zmax
            :finally (return (values xmin xmax ymin ymax zmin zmax)))
    (let* ((xavg (/ (+ xmax xmin) 2))
           (yavg (/ (+ ymax ymin) 2))
           (zavg (/ (+ zmax zmin) 2))
           (m (max (- xmax xmin) (- ymax ymin) (- zmax zmin))))
      (loop :for i :from 0 :below (length (vertices model)) :do
        (setf (aref (vertices model) i)
              (v* (v- (aref (vertices model) i) (vec xavg yavg zavg))
                  (/ 2 m))))))
  (setf (planes model) (calculate-planes model))
  model)

(defun write-model (s model)
  (loop :for v :across (vertices model) :do
    (format s "v ~a ~a ~a~%" (vx v) (vy v) (vz v)))
  (loop :for tr :across (triangles model) :do
    (format s "f ~a ~a ~a~%" (aref tr 0) (aref tr 1) (aref tr 2))))

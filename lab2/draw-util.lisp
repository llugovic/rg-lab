(in-package #:rg-lab2.exercise1)

(defmacro with-filled-gl-array ((arr-sym type contents) &body body)
  (let ((contents-sym (gensym "CONTENTS"))
        (count-sym (gensym "COUNT")))
    `(let* ((,contents-sym ,contents)
            (,count-sym (length ,contents-sym)))
       (gl:with-gl-array (,arr-sym ,type :count ,count-sym)
         (dotimes (i ,count-sym)
           (setf (gl:glaref ,arr-sym i)
                 (aref ,contents-sym i)))
         ,@body))))

(defun upload-buffer-data (target type hint data)
  (with-filled-gl-array (arr type data)
    (gl:buffer-data target hint arr)))

(defun vector-push-extend-vec2 (vec2 vector)
  (with-vec2 (x y) vec2
    (vector-push-extend x vector)
    (vector-push-extend y vector)))

(defun vector-push-extend-vec4 (vec4 vector)
  (with-vec4 (x y z w) vec4
    (vector-push-extend x vector)
    (vector-push-extend y vector)
    (vector-push-extend z vector)
    (vector-push-extend w vector)))

(defun vec3->array (v)
  (let ((array (make-array 3)))
    (setf (aref array 0) (vx v)
          (aref array 1) (vy v)
          (aref array 2) (vz v))
    array))

(defun vec4->array (v)
  (let ((array (make-array 4)))
    (setf (aref array 0) (vx v)
          (aref array 1) (vy v)
          (aref array 2) (vz v)
          (aref array 3) (vw v))
    array))

(defun vec2->vec3 (v)
  (vec (vx v) (vy v) 1.0))

(defun vec3->vec4 (v)
  (vec (vx v) (vy v) (vz v) 1.0))

(defun vec3-normalize (v)
  (with-vec3 (x y w) v
    (vec3 (/ x w) (/ y w) 1.0)))

(defun vec4-normalize (v)
  (with-vec4 (x y z w) v
    (vec4 (/ x w) (/ y w) (/ z w) 1.0)))

(defun vec4->vec3 (v)
  (vxyz (vec4-normalize v)))

(defun vec3->vec2 (v)
  (vxy (vec3-normalize v)))

(defun look-at (eye target up)
  (let* ((z (nvunit (v- eye target)))
         (x (nvunit (vc up z)))
         (y (vc z x)))
    (mat (vx3 x) (vy3 x) (vz3 x) (- (v. x eye))
         (vx3 y) (vy3 y) (vz3 y) (- (v. y eye))
         (vx3 z) (vy3 z) (vz3 z) (- (v. z eye))
         0 0 0 1)))

(defmacro with-vecs ((spec &rest specs) &body body)
  (if spec
      `(with-vec ,(butlast spec) ,@(last spec)
         (with-vecs (,(car specs) ,@(cdr specs))
           ,@body))
      `(progn ,@body)))

(defun lerp (x a b c d)
  (+ (* (/ (- x a) (- b a)) (- d c)) c))

(defun lerp-vec2 (v from-begin from-end to-begin to-end)
  (with-vecs ((x1 x2 v)
              (a1 a2 from-begin)
              (b1 b2 from-end)
              (c1 c2 to-begin)
              (d1 d2 to-end))
    (vec2 (lerp x1 a1 b1 c1 d1)
          (lerp x2 a2 b2 c2 d2))))

(defun lerp-vec4 (v from-begin from-end to-begin to-end)
  (with-vecs ((x1 x2 x3 x4 v)
              (a1 a2 a3 a4 from-begin)
              (b1 b2 b3 b4 from-end)
              (c1 c2 c3 c4 to-begin)
              (d1 d2 d3 d4 to-end))
    (vec4 (lerp x1 a1 b1 c1 d1)
          (lerp x2 a2 b2 c2 d2)
          (lerp x3 a3 b3 c3 d3)
          (lerp x4 a4 b4 c4 d4))))

(in-package #:rg-lab2.exercise1)

(defclass particle ()
  ((%start-life
    :reader start-life
    :accessor %start-life)
   (%life
    :initarg :life
    :accessor life)
   (%pos
    :initarg :pos
    :accessor pos)
   (%color
    :initarg :color
    :accessor color)
   (%velocity
    :initarg :velocity
    :accessor velocity)))

(defmethod initialize-instance :after ((particle particle) &key)
  (setf (%start-life particle) (life particle)))

(defmethod print-object ((particle particle) stream)
  (print-unreadable-object (particle stream :type t :identity t)
    (format stream "~s ~s ~s"
            (life particle) (pos particle) (color particle))))

(defparameter *particle-min-life* 300)
(defparameter *particle-max-life* 500)

(defun make-particle (pos velocity color)
  (make-instance 'particle
                 :life (floor
                        (lerp (random 1.0)
                              0 1
                              *particle-min-life*
                              *particle-max-life*))
                 :pos pos
                 :velocity velocity
                 :color color))

(defun life-ratio (particle)
  (let ((start (start-life particle)))
    (if (= start 0) 0 (/ (life particle) start))))

(defun update-particle (particle)
  (decf (life particle))
  (unless (minusp (life particle))
    (prog1 t
      (setf (pos particle) (v+ (pos particle) (velocity particle))))))

(defun renew-particle (particle pos velocity color)
  (reinitialize-instance particle
                         :life (floor
                                (lerp (random 1.0)
                                      0 1
                                      *particle-min-life*
                                      (start-life particle)))
                         :pos pos
                         :velocity velocity
                         :color color))

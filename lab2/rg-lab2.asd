(asdf:defsystem #:rg-lab2
  :description "RG Lab2"
  :author "Lovro Lugović <lovro.lugovic@fer.hr>"
  :license  "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on ("alexandria"
               "cl-opengl"
               "sdl2"
               "3d-vectors"
               "3d-matrices"
               "parse-float"
               "pngload")
  :components ((:file "package")
               (:file "util")
               (:file "draw-util")
               (:file "model")
               (:file "particle")
               (:file "draw")
               (:file "exercise1")))

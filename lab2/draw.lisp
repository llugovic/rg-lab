(in-package #:rg-lab2.exercise1)

(defparameter *width* 640)
(defparameter *height* 480)

(defparameter *vert-shader-src*
  "#version 330 core

uniform mat4 model_mat;
uniform mat4 view_mat;
uniform mat4 proj_mat;
uniform float life_ratio;

layout (location = 0) in vec4 pos;
layout (location = 1) in vec4 col;
layout (location = 2) in vec2 tex;

out vec4 frag_color;
out vec2 tex_coord;

void main()
{
    gl_Position = proj_mat * view_mat * model_mat *
                  vec4(pos.xyz * life_ratio, 1);
    frag_color = col;
    tex_coord = tex;
}")

(defparameter *frag-shader-src*
  "#version 330 core

in vec4 frag_color;
in vec2 tex_coord;
uniform float life_ratio;

uniform sampler2D my_texture;

void main()
{
    gl_FragColor = vec4(frag_color.xyz, life_ratio);
    vec4 sample = texture(my_texture, tex_coord);
    gl_FragColor = vec4(sample.xyz, min(sample.w, life_ratio));
}")

(defparameter *vao* nil)
(defparameter *vbo* nil)
(defparameter *program* nil)
(defparameter *vert-shader* nil)
(defparameter *frag-shader* nil)
(defparameter *image* nil)
(defparameter *texture* nil)

(defparameter *uniform-model-mat* nil)
(defparameter *uniform-view-mat* nil)
(defparameter *uniform-proj-mat* nil)
(defparameter *uniform-sampler* nil)
(defparameter *uniform-life-ratio* nil)

(defparameter *current-model* nil)
(defparameter *camera-eye* nil)
(defparameter *camera-target* nil)
(defparameter *camera-up* nil)
(defparameter *camera-distance* nil)
(defparameter *camera-param* nil)
(defparameter *camera-param-delta* (* 5 (/ pi 180)))

(defparameter *proj-half-width* 1)
(defparameter *proj-half-height* 1)
(defparameter *proj-z-near* 1)
(defparameter *proj-z-far* 10)

(defparameter *model-mat* (meye 4))
(defparameter *view-mat* nil)
(defparameter *proj-mat*
  (mfrustum (- *proj-half-width*) *proj-half-width*
            (- *proj-half-height*) *proj-half-height*
            *proj-z-near* *proj-z-far*))

(defparameter *z-buffering* nil)
(defparameter *polygon-mode-line* nil)

(defun random-vec4 (&optional from to)
  (let ((vec (vec4 (random 1.0) (random 1.0) (random 1.0) (random 1.0))))
    (if (not (and from to))
        vec
        (lerp-vec4 vec (vec4 0 0 0 0) (vec4 1 1 1 1) from to))))

(defun make-particles (n)
  (let ((particles (make-array n)))
    (prog1 particles
      (loop :for i :from 0 :below n :do
        (setf (aref particles i)
              (make-particle
               (random-vec4 (vec4 -0.5 0.5 -0.5 1)
                            (vec4 0.5 0.5 0.5 1))
               (vec4 0 -0.0025 0 0)
               (let ((vec (random-vec4)))
                 (setf (vw vec) 1)
                 vec)))))))

(defun update-particles ()
  (loop :for p :across *particles* :do
    (let ((alivep (update-particle p)))
      (unless alivep
        (renew-particle
         p
         (random-vec4 (vec4 -0.5 -0.5 -0.5 1)
                      (vec4 0.5 0.5 0.5 1))
         (vec4 0 -0.0025 0 0)
         (let ((vec (random-vec4)))
           (setf (vw vec) 1)
           vec))))))

(defparameter *num-particles* 500)
(defparameter *particles* (make-particles *num-particles*))

(defun setup-opengl ()
  (setf *vao* (gl:gen-vertex-array)
        *vbo* (gl:gen-buffer))

  (setf *program* (gl:create-program)
        *vert-shader* (gl:create-shader :vertex-shader)
        *frag-shader* (gl:create-shader :fragment-shader)
        *image* (pngload:load-file
                 (asdf:system-relative-pathname :rg-lab "texture/snow2.png")
                 :flatten t
                 :flip-y t)
        *texture* (gl:gen-texture))

  (gl:shader-source *vert-shader* *vert-shader-src*)
  (gl:shader-source *frag-shader* *frag-shader-src*)

  (gl:bind-vertex-array *vao*)
  (gl:enable-vertex-attrib-array 0)
  (gl:enable-vertex-attrib-array 1)
  (gl:enable-vertex-attrib-array 2)

  (gl:compile-shader *vert-shader*)
  (gl:compile-shader *frag-shader*)

  (unless (gl:get-shader *vert-shader* :compile-status)
    (error (format nil "Compilation failed for vert-shader:~%~a~%"
                   (gl:get-shader-info-log *vert-shader*))))
  (unless (gl:get-shader *frag-shader* :compile-status)
    (error (format nil "Compilation failed for vert-shader:~%~a~%"
                   (gl:get-shader-info-log *frag-shader*))))

  (gl:attach-shader *program* *vert-shader*)
  (gl:attach-shader *program* *frag-shader*)
  (gl:link-program *program*)

  (unless (gl:get-program *program* :link-status)
    (error (format nil "Linking failed for program:~%~a~%"
                   (gl:get-program-info-log *program*))))

  (gl:use-program *program*)

  (gl:active-texture 0)
  (gl:bind-texture :texture-2d *texture*)
  (gl:tex-parameter :texture-2d :texture-wrap-s :repeat)
  (gl:tex-parameter :texture-2d :texture-wrap-t :repeat)
  (gl:tex-parameter :texture-2d :texture-min-filter :linear)
  (gl:tex-parameter :texture-2d :texture-mag-filter :linear)
  (let ((data (pngload:data *image*))
        (w (pngload:width *image*))
        (h (pngload:height *image*)))
    (gl:tex-image-2d :texture-2d 0 :rgba w h 0 :rgba :unsigned-byte data))
  (gl:generate-mipmap :texture-2d)

  (setf *uniform-model-mat* (gl:get-uniform-location *program* "model_mat")
        *uniform-view-mat* (gl:get-uniform-location *program* "view_mat")
        *uniform-proj-mat* (gl:get-uniform-location *program* "proj_mat")
        *uniform-sampler* (gl:get-uniform-location *program* "my_texture")
        *uniform-life-ratio* (gl:get-uniform-location *program* "life_ratio"))

  (gl:uniformi *uniform-sampler* 0)
  (gl:uniformf *uniform-life-ratio* 1)

  (when *z-buffering*
    (gl:enable :depth-test))

  (when *polygon-mode-line*
    (gl:polygon-mode :front-and-back :line))

  (gl:enable :blend)
  (gl:blend-func :src-alpha :one-minus-src-alpha)

  (gl:enable :cull-face)
  (gl:cull-face :back))

(defun tear-opengl ()
  (gl:delete-shader *vert-shader*)
  (gl:delete-shader *frag-shader*)
  (gl:delete-program *program*))

(defun shader-send-mats ()
  (gl:uniform-matrix-4fv *uniform-model-mat* (marr4 *model-mat*))
  (gl:uniform-matrix-4fv *uniform-view-mat* (marr4 *view-mat*))
  (gl:uniform-matrix-4fv *uniform-proj-mat* (marr4 *proj-mat*)))

(defun reset-camera ()
  (setf *camera-distance* 3
        *camera-eye* (vec3 0 0 *camera-distance*)
        *camera-target* (vec3 0 0 0)
        *camera-up* (vec3 0 1 0)
        *camera-param* (/ (- pi) 2)
        *view-mat* (mlookat *camera-eye* *camera-target* *camera-up*))
  (shader-send-mats))

(defun keydown (keysym)
  (let ((scancode (sdl2:scancode keysym)))
    (wrap-case scancode
      (:scancode-h (incf *camera-param* *camera-param-delta*))
      (:scancode-l (decf *camera-param* *camera-param-delta*))
      (:scancode-j (incf *camera-distance* *camera-param-delta*))
      (:scancode-k (decf *camera-distance* *camera-param-delta*))
      (:scancode-r (reset-camera))
      (after
       (setf *camera-eye*
             (vec3 (* *camera-distance* (cos *camera-param*)) 0
                   (* *camera-distance* (sin *camera-param*)))
             *view-mat*
             (mlookat *camera-eye* *camera-target* *camera-up*))
       (shader-send-mats)))
    (case scancode
      (:scancode-y
       (setf *z-buffering* (not *z-buffering*))
       (if *z-buffering*
           (gl:enable :depth-test)
           (gl:disable :depth-test)))
      (:scancode-p
       (setf *polygon-mode-line* (not *polygon-mode-line*))
       (if *polygon-mode-line*
           (gl:polygon-mode :front-and-back :line)
           (gl:polygon-mode :front-and-back :fill))))))

(defun reshape (event data1 data2)
  ;; https://wiki.libsdl.org/SDL_WindowEventID
  ;; WindowEvent enumerations start at 0
  (when (= event 5)
    (setf *width* data1
          *height* data2))
  (when (or (= event 1) (= event 5))
    (let* ((min (min *width* *height*))
           (x (- (/ *width* 2) (/ min 2)))
           (y (- (/ *height* 2) (/ min 2))))
      (gl:viewport x y min min))))

(defun display (win model)
  (declare (ignore model))
  (gl:clear-color 0 0 0 1)
  (gl:clear :color-buffer :depth-buffer)

  (loop :for p :across *particles* :do
    (let* ((o (vxyz (pos p)))
           (z (vunit (v- *camera-eye* o)))
           (x (vunit (vc *camera-up* z)))
           (y (vc z x))
           (*model-mat*
             (m* (mtranslation o)
                 (mat4 (list
                        (vx x) (vx y) (vx z) 0
                        (vy x) (vy y) (vy z) 0
                        (vz y) (vz y) (vz z) 0
                        0 0 0 1)))))
      (shader-send-mats)
      (gl:uniformf *uniform-life-ratio* (life-ratio p))
      (gl:draw-arrays :triangles 0 6)))
  (update-particles)

  (sdl2:gl-swap-window win))

(defun idle (win model)
  #+swank
  (update-swank)
  (display win model))

(defun upload-quad ()
  (gl:bind-vertex-array *vao*)
  (gl:bind-buffer :array-buffer *vbo*)

  (let ((data (make-array 0 :element-type 'float
                            :adjustable t
                            :fill-pointer 0)))
    (vector-push-extend-vec4 (vec4 -1 -1 -1 1) data)
    (vector-push-extend-vec4 (vec4 1 -1 -1 1) data)
    (vector-push-extend-vec4 (vec4 -1 1 -1 1) data)
    (vector-push-extend-vec4 (vec4 1 1 -1 1) data)
    (vector-push-extend-vec4 (vec4 -1 1 -1 1) data)
    (vector-push-extend-vec4 (vec4 1 -1 -1 1) data)

    (vector-push-extend-vec4 (vec4 1 0 0 1) data)
    (vector-push-extend-vec4 (vec4 0 1 0 1) data)
    (vector-push-extend-vec4 (vec4 0 0 1 1) data)
    (vector-push-extend-vec4 (vec4 1 0 0 1) data)
    (vector-push-extend-vec4 (vec4 0 1 0 1) data)
    (vector-push-extend-vec4 (vec4 0 0 1 1) data)

    (vector-push-extend-vec2 (vec2 0 0) data)
    (vector-push-extend-vec2 (vec2 1 0) data)
    (vector-push-extend-vec2 (vec2 0 1) data)
    (vector-push-extend-vec2 (vec2 1 1) data)
    (vector-push-extend-vec2 (vec2 0 1) data)
    (vector-push-extend-vec2 (vec2 1 0) data)

    (upload-buffer-data :array-buffer :float :static-draw data)
    (gl:vertex-attrib-pointer 0 4 :float :false 0 0)
    (gl:vertex-attrib-pointer 1 4 :float :false 0 96)
    (gl:vertex-attrib-pointer 2 2 :float :false 0 192)))

(defun upload-model (model)
  (gl:bind-vertex-array *vao*)
  (gl:bind-buffer :array-buffer *vbo*)

  (let ((len (length (triangles model)))
        (data (make-array 0 :element-type 'float
                            :adjustable t
                            :fill-pointer 0)))
    (loop :for i :from 0 :below len
          :for tr = (get-triangle model i) :do
            (loop :for u :across tr
                  :for v = (vec3->vec4 u)
                  :do (vector-push-extend-vec4 v data)))
    (upload-buffer-data :array-buffer :float :static-draw data)
    (gl:vertex-attrib-pointer 0 4 :float :false 0 0)
    #+nil
    (gl:vertex-attrib-pointer 1 4 :float :false 0 (* len 3 4 4))))

(defun upload-particle-quad ()
  (gl:bind-vertex-array *vao*)
  (gl:bind-buffer :array-buffer *vbo*)

  (let ((data (make-array 0 :element-type 'float
                            :adjustable t
                            :fill-pointer 0)))
    (vector-push-extend-vec4 (vec4 -0.075 -0.075 0 1) data)
    (vector-push-extend-vec4 (vec4 0.075 -0.075 0 1) data)
    (vector-push-extend-vec4 (vec4 -0.075 0.075 0 1) data)
    (vector-push-extend-vec4 (vec4 0.075 0.075 0 1) data)
    (vector-push-extend-vec4 (vec4 -0.075 0.075 0 1) data)
    (vector-push-extend-vec4 (vec4 0.075 -0.075 0 1) data)

    (vector-push-extend-vec4 (vec4 1 0 0 1) data)
    (vector-push-extend-vec4 (vec4 1 0 0 1) data)
    (vector-push-extend-vec4 (vec4 1 0 0 1) data)
    (vector-push-extend-vec4 (vec4 1 0 0 1) data)
    (vector-push-extend-vec4 (vec4 1 0 0 1) data)
    (vector-push-extend-vec4 (vec4 1 0 0 1) data)

    (vector-push-extend-vec2 (vec2 0 0) data)
    (vector-push-extend-vec2 (vec2 1 0) data)
    (vector-push-extend-vec2 (vec2 0 1) data)
    (vector-push-extend-vec2 (vec2 1 1) data)
    (vector-push-extend-vec2 (vec2 0 1) data)
    (vector-push-extend-vec2 (vec2 1 0) data)

    (upload-buffer-data :array-buffer :float :static-draw data)
    (gl:vertex-attrib-pointer 0 4 :float :false 0 0)
    (gl:vertex-attrib-pointer 1 4 :float :false 0 96)
    (gl:vertex-attrib-pointer 2 2 :float :false 0 192)))

(defun draw-model (model)
  (sdl2:make-this-thread-main
   (lambda ()
     (let ((*current-model* model))
       (sdl2:with-init (:everything)
         (sdl2:with-window (win :title "Models" :flags '(:opengl)
                                :w *width* :h *height*)
           (sdl2:gl-set-attr :context-major-version 3)
           (sdl2:gl-set-attr :context-minor-version 3)

           (sdl2:with-gl-context (gl win)
             (sdl2:gl-make-current win gl)

             (setup-opengl)
             (reset-camera)

             ;; (upload-quad)
             ;; (upload-model model)
             (upload-particle-quad)

             (sdl2:with-event-loop (:method :poll)
               (:quit () t)
               (:keydown (:keysym keysym) (keydown keysym))
               (:windowevent (:event event :data1 data1 :data2 data2)
                             (reshape event data1 data2))
               (:idle () (idle win model)))

             (tear-opengl))))))))

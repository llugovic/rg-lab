(asdf:defsystem #:rg-lab
  :description "RG Laboratory Exercises"
  :author "Lovro Lugović <lovro.lugovic@fer.hr>"
  :license  "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on ("rg-lab1"
               "rg-lab2"
               "rg-lab3"))
